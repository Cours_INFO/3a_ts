#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 13:10:12 2022

@author: BONFILS Antoine
"""

import numpy as np #traitement
import matplotlib.pyplot as plt
import scipy.fftpack as sf
import scipy.signal.windows as signal

    #AMPLITUDES
A = 1
B = 1
    #NOMBRES D'ECHANTILLONS
N = 64

P=N

    #PERIODE D'ECHANTILLONNAGE
te = np.arange(0,N,1)   #debut, fin, step

    #FREQUENCE D'ECHANTILLONNAGE
fe = np.arange(0,1,1/P)   #debut, fin, step

    #FREQUENCES
f10 = 0.2       #entre 0 et 0.5
f20 = 0.25      #entre 0 et 0.5

    #SINUSOÎDES
sin1 = A*np.sin(2*np.pi*f10*te)
sin2 = B*np.sin(2*np.pi*f20*te)
sin3 = sin1 + sin2

FFTsin1 = sf.fft(sin1,P)
FFTsin2 = sf.fft(sin2,P)
FFTsin3 = sf.fft(sin3,P)

    #PLOT
plt.figure()
plt.subplots_adjust(left=0.08, right = 0.95, wspace = 0.25, hspace = 0.6)
plt.subplot(4,3,1),plt.plot(te, sin1), plt.title('Sinus1')
plt.subplot(4,3,2),plt.plot(te, sin2), plt.title('Sinus2')
plt.subplot(4,3,3),plt.plot(te, sin3), plt.title('Sinus3')
plt.subplot(4,3,4),plt.plot(fe, np.abs(FFTsin1)), plt.title('TF Sinus1')
plt.subplot(4,3,5),plt.plot(fe, np.abs(FFTsin2)), plt.title('TF Sinus2')
plt.subplot(4,3,6),plt.plot(fe, np.abs(FFTsin3)), plt.title('TF Sinus3')

    #PARAM
freq = sf.fftfreq(10*N, 1)
Porte = signal.boxcar(N)
FFTPorte = sf.fft(Porte,10*N)
Hamming = signal.hamming(N) 
FFTHamming =sf.fft(Hamming,10*N)
BlackMan = signal.blackman(N) 
FFTBlackman = sf.fft(BlackMan,10*N)

    #PLOT
plt.subplot(4,3,7),plt.stem(te, Porte), plt.title('Porte')
plt.subplot(4,3,7),plt.stem(te, Hamming), plt.title('Hamming')
plt.subplot(4,3,7),plt.stem(te, BlackMan), plt.title('Porte, Hamming, BlackMan')

plt.subplot(4,3,8),plt.plot(freq, np.abs(FFTPorte)), plt.title('TF Porte')
plt.subplot(4,3,8),plt.plot(freq, np.abs(FFTHamming)), plt.title('TF Hamming')
plt.subplot(4,3,8),plt.plot(freq, np.abs(FFTBlackman)), plt.title('TF Porte, TF Hamming, TF BlackMan')

sinusUltimate = 1*np.sin(2*np.pi*0.075*te) + 1*np.sin(2*np.pi*0.1*te) + 0.02*np.sin(2*np.pi*0.16*te) 