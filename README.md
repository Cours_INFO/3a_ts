# **Tous les Documents - Traitement du Signal**

Tous les travaux pratiques dans le cadre du cours de Traitement du Signal à **POLYTECH GRENOBLE**.

**[BONFILS Antoine](https://gitlab.com/AntoineBF)** (INFO3)

***

## **TABLE DES TPs**

| Sujet | Traités  | Codes |
|:-----:|:--------:|:-----:|
| [TP 1 - Transformée de Fourier, Corrélation](./TP1/TP1.pdf) | :heavy_check_mark: | [autocorrelation.py](./TP1/autocorrelation.py) et [impulsion.py](./TP1/impulsion.py) |
| [TP 2 - Echantillonnage, résolution et fenêtrage](./TP2/TP2_19.pdf) | :heavy_check_mark: | [Echantillonnage, résolution et fenêtrage](./TP2/TP2.py) |

***

### Legende:

+ :heavy_check_mark: : trait
+ :x: : non disponible

***