#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 15:17:26 2022

@author: BONFILS Antoine
"""

import numpy as np #traitement
import matplotlib.pyplot as plt
import scipy.fftpack as sf
import scipy.signal as signal

    #PARAM
A = 3
N = 256
f0 = 50
fe = 1000

t = np.arange(0, N/fe, 1/fe)
sq = A*signal.square(np.multiply(2*np.pi*f0,t))
xf = np.arange(0,1,1/N)

B = 2
s = B*np.sin(2*np.pi*f0*t)
y2 = sf.fft(xf)
sqcor = np.correlate(sq,sq,'same')
sqcortf = sf.fft(sqcor)
scor = np.correlate(s,s,'same')
b = np.random.randn(N)
ecart_type = 3
s_b = s+b
sq_b = sq + b
s_bcor =  np.correlate(s_b,s_b,'same')
sq_bcor = np.correlate(sq_b,sq_b,'same')

    #PLOT
plt.figure()
plt.subplots_adjust(left=0.08, right = 0.95, wspace = 0.25, hspace = 0.6)
plt.subplot(3,3,1),plt.plot(t,sq),plt.title('carré')
plt.subplot(3,3,2),plt.plot(t, sqcor),plt.title('autocorrelation carré')
plt.subplot(3,3,3),plt.plot(t, sqcortf),plt.title('densité spectrale')
plt.subplot(3,3,4),plt.plot(t, s), plt.title('sinus')
plt.subplot(3,3,5),plt.plot(t, scor), plt.title('autocorrelation de sinus')
plt.subplot(3,3,6),plt.plot(t, sq_b), plt.title('carre+bruit')
plt.subplot(3,3,7),plt.plot(t, s_b), plt.title('sinus+bruit')
plt.subplot(3,3,8),plt.plot(t, s_bcor), plt.title('autocorrelation de sinus+bruit')
plt.subplot(3,3,9),plt.plot(t, sq_bcor), plt.title('autocorrelation de carré+bruit')
