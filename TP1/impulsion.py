#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 15:17:36 2022

@author: BONFILS Antoine
"""

import numpy as np #traitement
import matplotlib.pyplot as plt
import scipy.fftpack as sf

    #PARAM
npoint = 32
taille_porte = 5
y1 = 0 * np.linspace(0,1,npoint)
value = 4;
"""
y1[value] = 1
y1[-value] = 1
"""

xf = np.arange(0,1,1/npoint)

for i in range (0, taille_porte):
    y1[i] = 1

y2 = sf.fft(y1)

    #PLOT
plt.figure()
plt.subplots_adjust(left=0.08, right = 0.95, wspace = 0.25, hspace = 0.6)
plt.subplot(3,2,1),plt.stem(y1)
plt.subplot(3,2,2),plt.stem(np.abs(y2)),plt.title('module')
plt.subplot(3,2,3),plt.stem(np.real(y2)),plt.title('partie reelle')
plt.subplot(3,2,4),plt.stem(np.imag(y2)),plt.title('partie imaginaire')
plt.subplot(3,2,5),plt.stem(np.angle(y2)),plt.title('phase')
plt.subplot(3,2,6),plt.plot(xf, np.abs(y2))